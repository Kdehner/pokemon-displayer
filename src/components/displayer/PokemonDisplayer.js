import React, { Component } from "react";
import { Col, Row, Container } from "react-bootstrap";

import { PokemonContext } from "../PokemonContext";
import PokemonCard from "../pokemon/PokemonCard";

const url = "https://pokeapi.co/api/v2/pokemon?limit=151";

class PokemonDisplayer extends Component {
  static contextType = PokemonContext;

  componentDidMount() {
    const { addPokemon } = this.context;

    const fetchPokemonsData = (pokemon) => {
      const url = pokemon.url;
      fetch(url)
        .then((response) => response.json())
        .then((pokemonData) => addPokemon(pokemonData));
    };

    const fetchPokemons = () => {
      fetch(url)
        .then((response) => response.json())
        .then((data) => {
          data.results.map((pokemon) => fetchPokemonsData(pokemon));
        });
    };

    fetchPokemons();
  }

  render() {
    const { pokemons } = this.context;
    return (
      <Container>
        <Row>
          <Col xs={12}>
            <Row className="justify-content-around">
              {pokemons.map((pokemon) => {
                return <PokemonCard key={pokemon.id} pokemon={pokemon} />;
              })}
            </Row>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default PokemonDisplayer;
