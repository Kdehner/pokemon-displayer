export const ADD_POKEMON = "ADD_POKEMONS";

const addPokemon = (pokemon, state) => ({
  pokemons: [...state.pokemons, pokemon],
});

export const pokemonReducer = (state, action) => {
  switch (action.type) {
    case ADD_POKEMON:
      return addPokemon(action.pokemon, state);
    default:
      return state;
  }
};
