import React, { useReducer, createContext } from "react";

import { ADD_POKEMON, pokemonReducer } from "./PokemonReducer";

export const PokemonContext = createContext();

export const PokemonProvider = (props) => {
  const defaultState = {
    pokemons: [],
  };

  const [state, dispatch] = useReducer(pokemonReducer, defaultState);

  const addPokemon = (pokemon) => {
    dispatch({ type: ADD_POKEMON, pokemon });
  };

  const { pokemons } = state;

  const providerValue = {
    pokemons,
    addPokemon,
  };

  return (
    <PokemonContext.Provider value={providerValue}>
      {props.children}
    </PokemonContext.Provider>
  );
};
