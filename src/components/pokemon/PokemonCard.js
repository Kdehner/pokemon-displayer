import React from "react";
import { Card, Row, ButtonGroup, Button } from "react-bootstrap";

import { typeColors } from "../../helpers";

const PokemonCard = (props) => {
  const { name, id, types } = props.pokemon;
  const imageUrl = `https://pokeres.bastionbot.org/images/pokemon/${id}.png`;
  return (
    <Card className="m-1" style={{ width: "18rem" }}>
      <Card.Img className="p-1" variant="top" src={imageUrl} />
      <Card.Body>
        <Card.Title className="text-center text-capitalize">{name}</Card.Title>
      </Card.Body>
      <Card.Footer>
        <Row className="justify-content-center">
          <ButtonGroup horizontal>
            {types.map((type) => {
              const typeName = type.type.name;
              return (
                <Button
                  key={`${id}-${name}`}
                  style={{
                    background: `#${typeColors[typeName]}`,
                    border: "none",
                  }}
                  className="text-capitalize"
                >
                  {typeName}
                </Button>
              );
            })}
          </ButtonGroup>
        </Row>
      </Card.Footer>
    </Card>
  );
};

export default PokemonCard;
