import React from "react";
import { Col } from "react-bootstrap";

const Header = () => {
  return (
    <Col xs={12}>
      <h1 className="text-center p-3">Pokedex: Pokemon Displayer App</h1>
    </Col>
  );
};

export default Header;
