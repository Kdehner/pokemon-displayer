import React from "react";
import { Row } from "react-bootstrap";

import { PokemonProvider } from "./components/PokemonContext";
import Header from "./components/Header";
import PokemonDisplayer from "./components/displayer/PokemonDisplayer";

function App() {
  return (
    <PokemonProvider>
      <Row className="justify-content-center">
        <Header />
        <PokemonDisplayer />
      </Row>
    </PokemonProvider>
  );
}

export default App;
